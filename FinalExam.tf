provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "FinalExam_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name        = "FinalExam-vpc"
    environment = "dev"
  }
}

resource "aws_subnet" "public_subnet" {
  count                   = 2
  vpc_id                  = aws_vpc.FinalExam_vpc.id
  cidr_block              = element(["10.0.1.0/24", "10.0.2.0/24"], count.index)
  availability_zone       = element(["us-east-1a", "us-east-1b"], count.index)
  map_public_ip_on_launch = true
  tags = {
    Name        = "public-subnet"
    environment = "dev"
  }
}

resource "aws_subnet" "private_subnet" {
  count             = 4
  vpc_id            = aws_vpc.FInalExam_vpc.id
  cidr_block        = element(["10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"], count.index)
  availability_zone = element(["us-east-1a", "us-east-1b", "us-east-1a", "us-east-1b"], count.index)
  tags = {
    Name        = "private-instance"
    environment = "dev"
  }
}

resource "aws_security_group" "bastion_sg" {
  name        = "bastion-sg"
  description = "Security group for bastion host"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "bastion-sg"
    environment = "dev"
  }
}

resource "aws_instance" "bastion" {
  ami             = "ami-0123456789abcdef0"
  instance_type   = "t2.micro"
  subnet_id       = aws_subnet.public_subnet[0].id
  security_groups = [aws_security_group.bastion_sg.name]
  key_name        = "your-key-name" # Replace with your SSH key pair name
  tags = {
    Name        = "bastion"
    environment = "dev"
  }
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnet[0].id
}

resource "aws_eip" "nat_eip" {
  vpc = true
}

output "bastion_public_ip" {
  value = aws_instance.bastion.public_ip
}
